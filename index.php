<?php
 # используется при автозагрузке
 require_once 'libs/bootstrap.php';
 require_once 'libs/controller.php';
 require_once 'libs/model.php';
 require_once 'libs/views.php';
 
 # загружаем классы из библиотек
 include_once 'libs/database.php';
 include_once 'libs/session.php';
 include_once 'libs/cache.php';
 require_once 'config/config.php';

 # конфигурационные файлы
 require_once 'config/paths.php';
 require_once 'config/database.php';
 $apps = new Bootstrap();
?>
