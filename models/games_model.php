<?php 
/**
 * 
 */
class Games_model extends Model
{
	
	public function __construct()
	{
		parent::__construct();
	}

	/* Возвращает все поля для игры по ID */
	/* Возвращает массив */
	public function getGame($id=0) {
		$sth = $this->db->prepare("SELECT * FROM games WHERE "
              . "id= :id");
		$sth->execute(array('id' => $id));
		$data = $sth->fetch();
		if ($sth->rowCount() != 0) {
			return $data;
		} else { 
			return FALSE;
		}
	}

	/* Возвращает все поля для всех игр */
	/* Возвращает массив */
	public function getAllGames() {
		$query = "SELECT * FROM games";
		$sth = $this->db->prepare($query);
		$sth->execute();
		return $sth->fetchAll();
	}
}