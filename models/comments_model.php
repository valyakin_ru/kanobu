<?php 
/**
 *  Модель комментариев
 */
class Comments_model extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	protected function data_protectionSql ($str) {
		return addslashes($str);
	}

/* Возвращает все комментарии по нужной игре */
/* Если есть кэш, то берет данные из кеша, иначе из базы с записью в кэш*/
	public function getCommentsByGame($id=0) {
		$cacheData = $this->cache->read("game$id"."_".CACHE_FILE);
		if (!$cacheData) {
			$limit = COMMENTS_LIMIT;
			/* чтобы избежать ассинхронной загрузки в JS подгружаем сразу и данные пользоваттеля*/
			$query="SELECT *, users.login as 'login'  FROM comments ".
				"LEFT JOIN users ON user_id = users.id ".
				"WHERE game_id= :id ORDER BY RAND() LIMIT $limit";
			/*$query="SELECT * FROM comments ".
				"WHERE game_id= :id ORDER BY RAND() LIMIT $limit";*/
			$sth = $this->db->prepare($query);
			$sth->execute(array('id' => $id));
			$data = $sth->fetchAll();
			if ($sth->rowCount() != 0) {
				$this->cache->write("game$id"."_".CACHE_FILE, $data);
				return $data;
			} else { 
				return FALSE;
			}
		} else {
			return $cacheData;
		}
	}

	/* Возвращает 1 комментарий по ID пользователя для требуемой игры по ID */
	/* Возвращает массив */
	public function getCommentByUser($user_id=0, $game_id=0) {
		$sth = $this->db->prepare("SELECT * FROM comments WHERE "
              . "user_id= :u_id AND game_id= :g_id");
		$sth->execute(array('u_id' => $user_id,
							'g_id' => $game_id, ));

		$data = $sth->fetch();
		if ($sth->rowCount() != 0) {
			return $data;
		} else { 
			return FALSE;
		}

	}

	/* Создает новый комментарий и обновляет кэш */
	public function create($user_id, $game_id, $comment_text) {
		if ($this->getCommentByUser($user_id, $game_id)) {
			session::set('msg','<div class="err_msg">У вас уже есть отзыв к этой игре!</div>');
			return FALSE;
		}
		$comment_text = $this->data_protectionSql($comment_text);
		$sql = "INSERT INTO comments (user_id, game_id, comment_text) VALUES (:u_id, :g_id, :c_text)";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':u_id', $user_id, PDO::PARAM_STR);
        $sth->bindParam(':g_id', $game_id, PDO::PARAM_STR);
        $sth->bindParam(':c_text', $comment_text, PDO::PARAM_STR);
		if ($sth->execute()) {
			$this->cache->delete("game$game_id"."_".CACHE_FILE);
			return TRUE;
		} else return FALSE;
	}

	/* Удаляет комментарий из БД по ID game_id нужен для определения кеш файла $user_id лишний*/
	public function delete($id, $game_id, $user_id) {
		if (session::get('loginid') != $user_id) {
			session::set('msg','<div class="err_msg">Вы не можете удалить чужой отзыв!</div>');
			return FALSE;
		}
		$sql = "DELETE FROM comments  WHERE id=:id";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':id', $id, PDO::PARAM_STR);
		if ($sth->execute()) {
			$this->cache->delete("game$game_id"."_".CACHE_FILE);
			return TRUE;
		}
		else return FALSE;
	}	

	/* Обновляет комментарий в базе данных удаляет Кеш файл */
	public function update($id, $user_id, $game_id, $comment_text) {
		if (session::get('loginid') != $user_id) {
			session::set('msg','<div class="err_msg">Вы не можете редактировать чужой отзыв!</div>');
			return FALSE;
		}
		$comment_text = $this->data_protectionSql($comment_text);
		$sql = "UPDATE comments SET user_id=:u_id, game_id=:g_id, comment_text=:c_text WHERE id=:id";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->bindParam(':u_id', $user_id, PDO::PARAM_STR);
        $sth->bindParam(':g_id', $game_id, PDO::PARAM_STR);
        $sth->bindParam(':c_text', $comment_text, PDO::PARAM_STR);
		if ($sth->execute()) {
			$this->cache->delete("game$game_id"."_".CACHE_FILE);
			return TRUE;
		}
		else return FALSE;
	}	

/* удаление кеша из браузера*/
	public function xhrDropCacheAllComments($games) {
		foreach ($games as $key => $value) {
			//echo json_encode($value);
			$this->cache->delete("game".$value['id']."_".CACHE_FILE);
		}
	}

	
}
