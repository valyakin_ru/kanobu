<?php 
/**
 *  модель пользователей
 */
class Users_model extends model
{
	
	public function __construct()
	{
		parent::__construct();
	}

/* Возвращает id, login и img 1-го пользователя по ID */
/* Возвращает массив */
	public function getUser($id=0) {
		$sth = $this->db->prepare("SELECT id, login, img FROM users WHERE "
              . "id= :id");
		$sth->execute(array('id' => $id));
		$data = $sth->fetch();
		if ($sth->rowCount() != 0) {
			return $data;
		} else { 
			return FALSE;
		}
		
	}

/* Возвращает id, login и img всех пользователей */
/* Возвращает массив */
	public function getAllUsers() {
		$sth = $this->db->prepare("SELECT id, login, img FROM users");
		$sth->execute();
		$data = $sth->fetchAll();
		if ($sth->rowCount() != 0) {
			return $data;
		} else { 
			return FALSE;
		}
		
	}


}