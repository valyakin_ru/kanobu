<?php

class Index_Model extends Model {

    function __construct() {
        parent::__construct();
        $sth = $this->db->prepare("SELECT * FROM games");
        $sth->execute();
        $this->data = $sth->fetchAll();
        // echo "------------INDEX_MODEL----------";
    }

}
