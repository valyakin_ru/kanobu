<?php

class Login_Model extends Model{

    function __construct() {
        parent::__construct();
    }
    /* Проверка пользователя и осуществление входа в систему */
    public function run() {
      $sth = $this->db->prepare("SELECT id, login FROM users WHERE "
              . "login= :login and password= MD5(:password)");  
      
      $sth->execute(array(
          'login' => $_POST['login'],
          'password' => $_POST['password']
      ));
      
      $count = $sth->rowCount().' ';
      if ($count>0) {
          $data = $sth->fetch();
          Session::init();
          Session::set('loggedIn', true);
          Session::set('loginname', $data['login']);
          Session::set('loginid', $data['id']);

          header('location: ../index');
      } else {
          header('location: ../login');
      }
    }
}