<?php 

/**
 *  Контроллер для пользователей
 */
class Users extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->LoadModel('users');
	}

	public function xhrGetUsernameById() {
		echo json_encode($this->model->getUser($_POST['user_id'])['login']);
	}
}