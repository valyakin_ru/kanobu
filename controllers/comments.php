<?php 
/**
 * 
 */
class Comments extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->modelGames = $this->LoadModel('games');
		$this->LoadModel('comments');
	}

	public function create() {
		$user_id = $_POST['user_id'];
		$game_id = $_POST['game_id'];
		$comment_text = strip_tags($_POST['review_text']);
		if ($comment_text == "") { 
			session::set('msg','<div class="err_msg">Отзыв не может быть пустым</div>');
			header("location: ".URL."games/view/$game_id/$user_id");
			 exit();
		}
		
		//$comment_text = htmlentities($_POST['review_text'], ENT_QUOTES, "UTF-8");
		//print_r($comment_text);
		//$comment_text = htmlspecialchars($_POST['review_text'], ENT_QUOTES);
		//print_r($comment_text);

		if ($this->model->create($user_id, $game_id, $comment_text)) {
			session::set('msg','<div class="info_msg">Добавилось удачно!</div>');
			header("location: ".URL."games/view/$game_id/$user_id");
		}
		else {
			if (!session::get('msg'))
				session::set('msg','<div class="err_msg">Произошла ошибка добавления!</div>');
			header("location: ".URL."games/view/$game_id/$user_id");
		}

	}

	public function delete($id, $game_id, $user_id) {
		//$id = $_POST['comment_id'];

		if ($this->model->delete($id, $game_id, $user_id)) {
					session::set('msg','<div class="info_msg">Удалилось удачно!</div>');
							header("location: ".URL."games/view/$game_id/".session::get('loginid'));
							exit();
		}
		else {
			if (!session::get('msg'))
			session::set('msg','<div class="err_msg">Произошла ошибка удаления!</div>');
			header("location: ".URL."games/view/$game_id/".session::get('loginid'));
			exit();
		}
	}



	public function update() {
		$id = $_POST['comment_id'];
		$user_id = $_POST['user_id'];
		$game_id = $_POST['game_id'];
		$comment_text = strip_tags($_POST['review_text']);

		if ($comment_text == "") { 
			session::set('msg','<div class="err_msg">Отзыв не может быть пустым</div>');
			 header("location: ".URL."games/view/$game_id/".session::get('loginid'));
			 exit();
		}
		//$comment_text = htmlentities($_POST['review_text'], ENT_QUOTES, "UTF-8");
		//print_r($comment_text);
		//$comment_text = htmlspecialchars($_POST['review_text'], ENT_QUOTES);
		//print_r($comment_text);
		if ($this->model->update($id, $user_id, $game_id, $comment_text)) { 
					session::set('msg','<div class="info_msg">Обновилось удачно!</div>');
					header("location: ".URL."games/view/$game_id/".session::get('loginid'));
		}
		else {
			if (!session::get('msg'))
			session::set('msg','<div class="err_msg">Произошла ошибка обновления!</div>');
			header("location: ".URL."games/view/$game_id/".session::get('loginid'));
		}
	}

/* Функция для обработки запроса на удаление кеша из браузера*/
	public function xhrDropCacheAllComments() {
		$this->model->xhrDropCacheAllComments($this->modelGames->getAllGames());
	}
}
