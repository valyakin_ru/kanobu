<?php 

/*
Контроллер управления страницей игры с комментариями
*/
class Games extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->users_model = $this->LoadModel('users');
		$this->comments_model=$this->LoadModel('comments');
		$this->LoadModel('games');
	}

	public function view($game_id = NULL, $user_id = NULL) {
		
		$game_id = $_POST['game_id'] ?? $game_id;
		$user_id = $_POST['user_id'] ?? $user_id;
		$this->view->game_id = $game_id;
		$this->view->user_id = $user_id;
		$this->view->game = $this->model->getGame($game_id);
		$this->view->user = $this->users_model->getUser($user_id); 
		
		$this->view->userComment = $this->comments_model->getCommentByUser($user_id,$game_id);
		$this->view->comments = $this->comments_model->getCommentsByGame($game_id);
		$this->view->users_model = $this->users_model;
		if ($this->view->game)
			$this->view->render('games/index');
		else {
			$this->view->msg = $pageName." - такой страницы не существует";
        	$this->view->render('error/index');
		} 

	}

	public function xhrGetParams($game_id = NULL, $user_id = NULL) {
		$game_id = $game_id ?? $_POST['game_id'];
		$user_id = $user_id ?? $_POST['user_id'];

		$this->view->game = $this->model->getGame($game_id);
		$this->view->user = $this->users_model->getUser($user_id); 
		$this->view->userComment = $this->comments_model->getCommentByUser($user_id,$game_id);
		$this->view->comments = $this->comments_model->getCommentsByGame($game_id);
		$this->view->users_model = $this->users_model;
		echo json_encode($this->view);
	}
}
