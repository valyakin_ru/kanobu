<?php
/*
Контроллер управления главной страницей
*/

 class Index extends Controller {
   
   public function __construct(){
       parent::__construct();
       // echo "Мы в контроллере Index";
       $this->LoadModel('index'); #Здесь загружаем модель, иначе если в бутстрампе, то сначала пройдет весь рендер
       $this->view->put_data = $this->model->data;
       //print_r($this->view->put_data);
       #$this->view->render('index/index');
       
   }  
   function index () {
       $this->view->render('index/index');
   }
 }
 
?>
