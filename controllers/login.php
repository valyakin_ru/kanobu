<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login extends Controller {

    function __construct() {
        parent::__construct();
        $this->LoadModel('login');
    }
    function Index() {
        $this->LoadModel('users');
        $this->view->users = $this->model->getAllUsers();

        $this->view->render('login/index');
    }
    function Run() {
        
        $this->model->run();
    }
    function logout() {
        //echo 'LogOUT!!!';
        session::set('loggedIn',false);
        session::set('loginname','');
        session::set('loginid','0');
        header('location: '.URL.'login');
        exit;
    }

}