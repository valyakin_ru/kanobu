<script>var game_id = "<?php echo $this->game_id; ?>";</script>
<script>var user_id = "<?php echo $this->user_id; ?>";</script>
<script>var ttl_cache = "<?php echo TTL_CACHE; ?>";</script>
<script>var ttl_change_activeblock = "<?php echo TTL_CHANGE_ACTIVEBLOCK; ?>";</script>
<script>var div_container = "<?php echo DIV_CONTAINER; ?>";</script>

<div class="content">
	<h1><?php echo $this->game['name']; ?></h1>
	
	<hr>
	
	<div class="description_game">
		<img src="<?php echo $this->game['poster']; ?>">
		<?php echo $this->game['discribe']; ?>
	</div>
	<hr>
	<h2>Отзывы об <?php echo $this->game['name']; ?></h2>
	<?php echo Session::get('msg'); Session::unset('msg'); ?>
	
		
	<?php if ($this->userComment): ?>
		Ваш отзыв
		<div class="reviews_active">
			<div class="active_name">
				<img class="face" src="<?php echo $this->users_model->getUser($this->userComment['user_id'])['img']; ?>" alt="">
				<?php echo $this->users_model->getUser($this->userComment['user_id'])['login']; ?>
			</div>
			<div id="comment" class="review_text">
				<?php echo $this->userComment['comment_text']; ?>
			</div>
			<button id="changeBtn" class="btn-info">Изменить</button>
			<div class="send">
				<form method="post" action="<?php echo URL?>comments/update" id="review">
					<input type="hidden" name="comment_id" value="<?php echo $this->userComment['id']; ?>">
					<input type="hidden" name="user_id" value="<?php echo $this->userComment['user_id']; ?>">
        			<input type="hidden" name="game_id" value="<?php echo $this->userComment['game_id']; ?>">
					<textarea placeholder="Напишите здесь ваш отзыв" name="review_text"><?php echo $this->userComment['comment_text']; ?></textarea>
					<input type="submit" value="отправить">
					<input type="submit" value="удалить" class="btn-info" id="deleteBtn" href="<?php echo URL?>comments/delete/<?php echo $this->userComment['id']; ?>/<?php echo $this->userComment['game_id']; ?>/<?php echo $this->userComment['user_id']; ?>">
				</form>
			</div>
			
		</div>
	<?php endif ?>

		<?php if (is_array($this->comments)): ?>
			<div id="<?php echo DIV_CONTAINER; ?>"></div>
		<?php elseif ($this->user['id']): ?>
			<div class="review_text">
				Еще никто ничего не написал. Будь Первым!
				<hr>
			</div>
		<?php endif ?>

		<?php if ($this->user['id'] && !$this->userComment): ?>
			<div class="send">
				<div class="default_msg">Вы можете оставить свой отзыв</div>
				<form method="post" action="<?php echo URL?>comments/create" id="review1">
					<input type="hidden" name="user_id" value="<?php echo $this->user['id']; ?>">
        			<input type="hidden" name="game_id" value="<?php echo $this->game['id']; ?>">
					<textarea placeholder="Напишите здесь ваш отзыв" name="review_text"></textarea>
					<input type="submit" value="отправить">
				</form>
			</div>
		<?php elseif (!$this->user['id']) : ?>
			<div class="msg-warning">Войдите, чтобы оставить комментарий <a href="<?php echo URL?>login">Войти</a></div>
		<?php endif ?>
</div>

<script type="text/javascript" src="<?php echo URL?>views/games/js/default.js"> </script>