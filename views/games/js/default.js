div_container = '#'+div_container;
/*Загружаем блок комметариев*/
$loadComments = function() {
	$(div_container).empty();
	$.ajax({
			url: 'http://projects.valyakin.ru/KanobuTest/games/xhrGetParams',
			method: 'post',
			dataType: 'json',
			data: {
				user_id: user_id,
				game_id: game_id
			},
			success: function(data){
				for (var i = 0; i<data.comments.length; ++i) {
					$(div_container).append('<div class="loaded" id="comment'+i+'">'+
												'<div class="reviews">'+
													'<div class="review_name">'+
														'<img class="face" src="" alt="">'+
													'</div>'+
													'<div class="review_text">'+
														'Комментарий'+
													'</div>'+
												'</div>'+
											 '</div>');
					/* Загружаем полученные данные в сформированный блок с классом loaded */
					/*$("#comment"+i).find('.face').prop("src", data.comments[i]['img']);
					$("#comment"+i).find('.review_name').append(data.comments[i]['login']);
					$("#comment"+i).find('.review_text').text(data.comments[i]['comment_text']);*/
				}

				/*Функция смешивания элементов массива */
				function shuffle(array) {
 				 array.sort(() => Math.random() - 0.5);
				}

				/*Чтобы при обновлении страницы независимо от инфы в кэш был новый порядок отзывов смешиваем полученный массив*/
				shuffle(data.comments);

				/* Загружаем полученные данные в сформированный блок с классом loaded */
				for (i = 0; i<data.comments.length; ++i) {
					$("#comment"+i).find('.face').prop("src", data.comments[i]['img']);
					$("#comment"+i).find('.review_name').append(data.comments[i]['login']);
					$("#comment"+i).find('.review_text').text(data.comments[i]['comment_text']);
				}
			/*Задаем подсветку для первого комментария*/
			    $('#comment0').toggleClass('active_comment', true);		
			}
		});



}


$(document).ready(function(){
	
	$loadComments();

	/*скрывам блок редактирования комметав*/
	$('#review').hide();

	/*вешаем собитие на кнопку и скрываем кнопку и показывам блок редактирования объявления*/
	$('#changeBtn').click(function() {
		$('#review').show();
		$("textarea").focus();
		$('#changeBtn').hide();
		$('#comment').hide();
	})
	$('#deleteBtn').click(function() {
		var url=$('#deleteBtn').attr('href');
		$('#review').attr('action', url);
	})

	/* устанавливаем количество знаков в textarea 250*/
	$("textarea").keyup(function() {
	    if (this.value.length > 250)
	        this.value = this.value.substr(0, 250);
	});

	/* задаем интервал для фокусировки*/
	setInterval(function() {
		
	    var elems = $(".loaded");
	    var elemsTotal = elems.length;
	    for(var i=0; i<elemsTotal; ++i){
	    	var classList = $('[id=comment'+i+']').attr('class').split(/\s+/);
			$.each(classList, function(index, item) {
			    if (item === 'active_comment') {
			        $('#comment'+i).toggleClass('active_comment', false);
			        ++i;
			        if (i == elemsTotal) {i = 0;}
			        $('#comment'+i).toggleClass('active_comment', true);
			        
			    }
			});
	    }
	}, ttl_change_activeblock); 



	setInterval(function() {
		$.post('http://projects.valyakin.ru/KanobuTest/comments/xhrDropCacheAllComments', function(){
			// тут можно вызвать функцию обновления блоков комментариев
			$loadComments();
		});

	}, ttl_cache);


});


/* $(document).ready(function(){

    var elems = $(".loaded");
    var elemsTotal = elems.length;
    for(var i=0; i<elemsTotal; ++i){$(elems[i]).attr('id', i)}
    	// Нужно добавить класс
    $('#comment0').toggleClass('active_comment', true);

 })*/

	

