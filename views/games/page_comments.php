
		<?php if ($this->userComment): ?>
			Ваш отзыв
			<div class="reviews">
				<div class="active_name">
					<?php echo $this->users_model->getUser($this->userComment['user_id'])['login']; ?>
				</div>
				<div id="comment" class="review_text">
					<?php echo $this->userComment['comment_text']; ?>
				</div>
				<button id="changeBtn" class="btn-info">Изменить</button>
				<div class="send">
					<form method="post" action="<?php echo URL?>comments/update" id="review">
						<input type="hidden" name="comment_id" value="<?php echo $this->userComment['id']; ?>">
						<input type="hidden" name="user_id" value="<?php echo $this->userComment['user_id']; ?>">
	        			<input type="hidden" name="game_id" value="<?php echo $this->userComment['game_id']; ?>">
						<textarea placeholder="Напишите здесь ваш отзыв" name="review_text"><?php echo $this->userComment['comment_text']; ?></textarea>
						<input type="submit" value="отправить">
						<button class="btn-info" id="deleteBtn" href="<?php echo URL?>comments/delete/<?php echo $this->userComment['id']; ?>/<?php echo $this->userComment['game_id']; ?>/<?php echo $this->userComment['user_id']; ?>">удалить</a>
					</form>
				</div>
				
			</div>
		<?php endif ?>
		<?php if (is_array($this->comments)): ?>
		<?php foreach ($this->comments as $key => $value): ?>
			<div class="reviews">
				<div class="review_name">
					<?php echo $this->users_model->getUser($value['user_id'])['login']; ?>
				</div>
				<div class="review_text">
					<?php echo $value['comment_text']; ?>
				</div>
			</div>
		<?php endforeach ?>
		<?php else: ?>
			<div class="review_text">
				Еще никто ничего не написал.
				<hr>
			</div>
		<?php endif ?>
		<?php if ($this->user['id'] && !$this->userComment): ?>
			<div class="send">
				<div class="default_msg">Вы можете оставить свой отзыв</div>
				<form method="post" action="<?php echo URL?>comments/create" id="review1">
					<input type="hidden" name="user_id" value="<?php echo $this->user['id']; ?>">
        			<input type="hidden" name="game_id" value="<?php echo $this->game['id']; ?>">
					<textarea placeholder="Напишите здесь ваш отзыв" name="review_text"></textarea>
					<input type="submit" value="отправить">
				</form>
			</div>
		<?php elseif (!$this->user['id']) : ?>
			<div class="msg-warning">Войдите, чтобы оставить комментарий <a href="<?php echo URL?>login">Войти</a></div>
		<?php endif ?>