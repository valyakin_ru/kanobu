<?php 
define ('COMMENTS_LIMIT', 10);							// Количество отзывов подгружаемых в контейнер
define ('CACHE_FILE', 'comments'.COMMENTS_LIMIT.'.txt');// Задаем имя файла для кеширования
define ('TTL_CACHE', '180000');							// Задаем время частоты обновления кэш для блока комментариев
define ('TTL_CHANGE_ACTIVEBLOCK', '10000');				// Задаем время для перехода подсветки на другой блок отзыва
define ('DIV_CONTAINER', 'fastopinion');				// Наименование контейнера для загрузки отзывов
