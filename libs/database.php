<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Database extends PDO {

    public function __construct() {
        //echo 'мы в Датабазе <br />';
        // Параметры беруться из database.php в папке config
        $dsn = DB_TYPE.":host=".DB_HOST.";dbname=".DB_NAME.";charset=".DB_CHARSET;
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        //$this->db = new PDO ($dsn, $user, $pass, $opt);
        parent::__construct($dsn, DB_USER, DB_PASS, $opt);  
    }

}