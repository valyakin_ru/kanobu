<?php

class Bootstrap {

    function __construct() {
        // echo ' Загружаемся....';
        $url = isset($_GET['route']) ? $_GET['route'] : Null;
        session:: init();
        //session:: destroy();

        $this->pageName = $url;
        $url = rtrim($url, '/');
        $url = explode('/', $url);

        # если урл пуст, значит созадаем контроллер Index
        if (empty($url[0])) {
            require_once 'controllers/index.php';
            $controller = new Index;
            //$controller->LoadModel('index');
            $controller->Index();
            return false; # дальше нам ничего не нужно - выходим
        }
        # если есть страница то создаем объект контроллер с именем этой страницы
        $file = 'controllers/' . $url[0] . '.php';
        if (file_exists($file)) {
            require_once $file;
            $controller = new $url[0];
            //$controller->LoadModel($url[0]);
        } else {
            # если такого контроллера у нас нет, то выводим ошибку
            $this->error();
            return false; # и выходим из бутстрапа
        }

        if (isset($url[4])) { # если четвертый параметр существует, то (нулевой у нас всегда указывает на контроллер)
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2],$url[3],$url[4]); # вызываем метод для контроллера с параметром url.ru/контроллер/метод/параметр
                # например   help/other/параметр,  где метод other() прописаны в контроллере help 
            }  
        }

        if (isset($url[3])) { # если третий параметр существует, то
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2],$url[3]); # вызываем метод для контроллера с параметром url.ru/контроллер/метод/параметр
                # например   help/other/параметр,  где метод other() прописаны в контроллере help 
            }  
        }
        if (isset($url[2])) { # если второй параметр существует, то
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}($url[2]); # вызываем метод для контроллера с параметром url.ru/контроллер/метод/параметр
                # например   help/other/параметр,  где метод other() прописаны в контроллере help 
            }
        } else { # иначе 
            if (isset($url[1])) { 
                #echo {$url[1]};
                if (method_exists($controller, $url[1])) {# например index/details или help/other/параметр, 
                # где методы details() и метод other() прописаны в контроллере index и help соответственно
                    $controller->{$url[1]}();
                } else {
                    echo 'errrrrrr. Нет такого метода';
                }
            } else {
                $controller->Index();
            }
        }
    }

    function error() {
        require_once 'controllers/error.php';
        $controller = new thisError($this->pageName);
        $controller->Index();
    }

}
