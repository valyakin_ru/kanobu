<?php 
class Cache {

	    function read($fileName) {

		    $fileName = 'cache/cache_'.$fileName;

		    if (file_exists($fileName)) {
		        $handle = fopen($fileName, 'rb');
		         if (filesize($fileName) == 0) {
	 				fclose($handle);
			        return null;
		         } else {
			        $variable = fread($handle, filesize($fileName));
			        fclose($handle);
			        return unserialize($variable);}
		    } else {
		        return null;
	    	}
		}

	    function write($fileName,$variable) {
	        $fileName = 'cache/cache_'.$fileName;
	        $handle = fopen($fileName, 'w');
	        fwrite($handle, serialize($variable));
	        fclose($handle);
	        /*file_put_contents($fileName, serialize($data), LOCK_EX);*/
	    }

	    function delete($fileName) {
	        $fileName = 'cache/cache_'.$fileName;
	        @unlink($fileName);
	    }
}