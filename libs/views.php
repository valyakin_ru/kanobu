<?php

class View {

    function __construct() {
        # echo 'Мы внутри View <br />';
    }
    public function render($name, $noInclude = false) {
        $this->getStyle();
        if ($noInclude) {
            include_once 'views/'.$name.'.php';
        } else {
        require_once 'views/header.php';
        include_once 'views/'.$name.'.php';
        require_once 'views/footer.php';
        }
    }
    function getStyle() {
        //session::init();
        $this->css_url=session::get('CssStyle') ?? 'public/css/old_style.css';
    }

}
