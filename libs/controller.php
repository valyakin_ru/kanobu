<?php

 class Controller {
    public $view;
    public function __construct() {
        # echo 'Мы внутри главного контроллера <br>'; 
        $this->view = new View();
        $this->view->userName = Session::get('loginname');
        $this->view->userId = Session::get('loginid');
    }
    public function LoadModel($name) {
        $path = 'models/'.$name.'_model.php';  
        if (file_exists($path)) {
            require_once $path;
            $modelName = $name.'_model';
            $this->model =  new $modelName;
            return $this->model;
        }
        
    }
 }
